<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New/Edit Contact</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body bgcolor="#80dfff">
<div align="center">
<div class="container">
<div class="jumbotran">
  
<table border="1" style="border-collapse:collapse" class="table table-striped">
<tr><th>Id</th><th>Name</th><th>Code</th><th>Edit</th><th>Delete</th><th>Fetch</th></tr>
<c:forEach var="coun" items="${country}">   
   <tr>
<td>${coun.id}</td>
<td>${coun.name}</td>
<td>${coun.code}</td>
<td><a href="/update?id=${coun.id}"><img src="/images/edit.png" width="20" height="20"/></a></td>
<td><a href="/delete?id=${coun.id}"><img src="/images/delete.png" width="20" height="20"/></a></td>
<td><a href="/greater?id=${coun.id}">Greater Than</a>&nbsp;&nbsp;<a href="/lesser?id=${coun.id}">Less Than</a></td>
</tr>  
</c:forEach>
</table>
<a href="/listpage/1">1</a>&nbsp;&nbsp;|&nbsp;&nbsp; 
<a href="/listpage/2">2</a>&nbsp;&nbsp;|&nbsp;&nbsp; 
<a href="/listpage/3">3</a>&nbsp;&nbsp;|&nbsp;&nbsp; 
<a href="/listpage/4">4</a>&nbsp;&nbsp;|&nbsp;&nbsp;</br> 
<a href="/">Register</a>
</div>
</div>
</div>
</body>
</html>
<!-- onclick="return confirm()" -->