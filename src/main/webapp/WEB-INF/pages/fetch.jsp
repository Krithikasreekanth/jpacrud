<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New/Edit Contact</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body bgcolor="#80dfff">
<div align="center">
<div class="container">
<div class="jumbotran">
  
<table border="1" style="border-collapse:collapse" class="table table-striped">
<tr><th>Id</th><th>Name</th><th>Code</th></tr>
<c:forEach var="coun" items="${country}">   
   <tr>
<td>${coun.id}</td>
<td>${coun.name}</td>
<td>${coun.code}</td>
</tr>  
</c:forEach>
</table>  
<a href="viewform">Go to back page</a>
</div>
</div>
</div>
</body>
</html>