package com;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CountryRepository extends CrudRepository<CountryModel,Integer>,PagingAndSortingRepository<CountryModel, Integer> {
	CountryModel findById(int id);
//	boolean existsById(int id);

	CountryModel findByName(String name);

	List<CountryModel> findByIdLessThan(int id);
	List<CountryModel> findByIdGreaterThan(int id);
//	public interface CountryRepository extends JpaRepository<CountryModel,Integer> 

	/*Page<CountryModel> findAll(Pageable pageable);*/

	
	

}
