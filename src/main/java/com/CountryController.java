package com;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;



@RestController

public class CountryController {
	@Autowired
	CountryRepository countryRepository;
@RequestMapping(value="/{id}",produces=MediaType.APPLICATION_XML_VALUE)
public CountryModel Id(@PathVariable int id){
	return countryRepository.findById(id);
	
}
//@RequestMapping(value="/{id1}")
//	public boolean existId(@PathVariable int id1)
//	{
//	return countryRepository.existsById(id1);	
//	}
@RequestMapping(value="/count")
public long count(){
	return countryRepository.count();
}
@RequestMapping(value="/")
public ModelAndView save(@ModelAttribute("country") CountryModel country){

	return new ModelAndView("addform");
}

@RequestMapping(value = "/save")
public ModelAndView getSave(@ModelAttribute("country") CountryModel country) {
    	 countryRepository.save(country);
    	 return new ModelAndView("redirect:viewform"); 
         
}
@RequestMapping(value = "/viewform")
public ModelAndView getAllCities(@ModelAttribute("country") CountryModel country) {
         List<CountryModel> li=(List<CountryModel>) countryRepository.findAll();
         return new ModelAndView("viewform","country",li);
}
@RequestMapping(value = "/update")
public ModelAndView update(@ModelAttribute("country") CountryModel country,HttpServletRequest req) {
	int id=Integer.parseInt(req.getParameter("id"));
      CountryModel country1 = countryRepository.findById(id);
      return new ModelAndView("addform","country",country1);
}


@RequestMapping(value="/delete")
public ModelAndView delete(HttpServletRequest req){
	int id=Integer.parseInt(req.getParameter("id"));
	 countryRepository.delete(id);
	 return new ModelAndView("redirect:viewform"); 
     
}
@RequestMapping(value="/lesser")
public ModelAndView findByIdLessThan(HttpServletRequest request){
	int id = Integer.parseInt(request.getParameter("id"));         
  List<CountryModel> list= countryRepository.findByIdLessThan(id);
  return new ModelAndView("fetch","country",list);
    
}
@RequestMapping(value="/greater")
public ModelAndView findByIdGreatThan(HttpServletRequest request){
	int id = Integer.parseInt(request.getParameter("id"));         
  List<CountryModel> list= countryRepository.findByIdGreaterThan(id);
  return new ModelAndView("fetch","country",list);
    
}

/*@RequestMapping(value="/greater/{id}")
public List<CountryModel> findByIdGreaterThan(@PathVariable int id){
	 
	return  (List<CountryModel>) countryRepository.findByIdGreaterThan(id);
    
}
@RequestMapping(value = "/{name}")
public CountryModel findByName(@PathVariable String name) {
      return countryRepository.findByName(name);
}*/
/*@RequestMapping(value="/pagecount")
Page<CountryModel> countryPageable(Pageable pageable) {
	return countryRepository.findAll(pageable);
}*/

public PageRequest gotopage(int page)
{
	PageRequest p=new PageRequest(page, 3);
	return p;
}
	@RequestMapping(value="/listpage/{page}")
	public ModelAndView pages(@PathVariable int page){
		Iterable<CountryModel> c=countryRepository.findAll(gotopage(page-1));
		List<CountryModel> list=iterabletoCollection(c);
		return new ModelAndView("viewform","country",list);
		/*List<CountryModel> list=new ArrayList<CountryModel>();
		for(CountryModel count:c)
		{
			list.add(count);
		}
		return new ModelAndView("viewform","country",list);*/
	}
	public List<CountryModel> iterabletoCollection(Iterable<CountryModel> c) {
		List<CountryModel> l=new ArrayList<CountryModel>();
		for(CountryModel count:c)
		{
			l.add(count);
		}
		return l;
	}
}